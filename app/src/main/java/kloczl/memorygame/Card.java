package kloczl.memorygame;

/**
 * Created by Klöczl on 2018.01.16..
 */

public class Card {
    private Boolean cardMatched = false;
    private final int cardID;
    private final int imageID;
    private final int baseImageID = R.drawable.card_background;
    private boolean turned = false;

    public Card(int imageID, int cardID){
        this.imageID = imageID;
        this.cardID = cardID;
    }

    public int getCardID(){
        return cardID;
    }

    public int getImageID(){
        return imageID;
    }

    public Boolean getCardMatched(){
        return cardMatched;
    }

    public void setCardMatched(boolean value){
        cardMatched = value;
    }

    public boolean getTurned(){
        return turned;
    }

    public void setTurned(boolean value){
        turned = value;
    }

    public int getBaseImageID(){
        return baseImageID;
    }
}
