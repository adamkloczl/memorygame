package kloczl.memorygame;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by Klöczl on 2018.01.14..
 */


public class CardAdapter extends BaseAdapter {

    private Context context;
    private final ArrayList<Card> cards;
    private LayoutInflater layoutInflater;

    public CardAdapter(Context context, ArrayList<Card> cards) {
        this.context = context;
        this.cards = cards;
    }

    public void sleep(){
        try { Thread.sleep(1000); }
        catch (InterruptedException ex)
        { android.util.Log.d("YourApplicationName", ex.toString()); }
    }

    @Override
    public int getCount() {
        return cards.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, final View convertView, ViewGroup viewGroup) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final ImageView imageView;
        if(convertView == null){
            imageView = new ImageView(context);
            imageView.setLayoutParams(new GridView.LayoutParams(240, 240));
            imageView.setPadding(10, 10, 10, 10);
            imageView.setAdjustViewBounds(true);
        }
        else{
            imageView =(ImageView) convertView;
        }
        if(cards.get(i).getTurned() == false){
            imageView.setImageResource(cards.get(i).getBaseImageID());
        }
        else{
            imageView.setImageResource(cards.get(i).getImageID());
        }
        return imageView;
    }
}

