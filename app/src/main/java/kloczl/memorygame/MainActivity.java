package kloczl.memorygame;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    //setting up some variables
    Context context = this;
    private GridView gridView;
    private int gameScore = 0, matchCount = 0, userScore;
    private Boolean firstFlip = true, flipping = false;
    private Button highScoreButton, confirmButton;
    private TextView currentScore, finalText, highScore;
    private EditText editText;
    Toast toast;
    static CardAdapter cardAdapter;
    private ArrayList<Integer> images = new ArrayList<>();
    static ArrayList<Card> cards = new ArrayList<>();
    Card card1, card2;
    String userName;
    final Handler handler = new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //giving these variables values
        gridView = findViewById(R.id.gridView);
        highScoreButton = findViewById(R.id.highScoreButton);
        currentScore = findViewById(R.id.currentScore);
        editText = findViewById(R.id.editText);
        finalText = findViewById(R.id.finalText);
        confirmButton = findViewById(R.id.newGame);
        highScore = findViewById(R.id.highScore);

        finalText.setVisibility(View.INVISIBLE);
        editText.setVisibility(View.INVISIBLE);
        confirmButton.setVisibility(View.INVISIBLE);
        highScore.setVisibility(View.INVISIBLE);

        //manipulating the arrays
        populateArrays();
        Collections.shuffle(images);
        populateCardArray();

        //attaching the adapter to the gridView
        cardAdapter = new CardAdapter(this, cards);
        gridView.setAdapter(cardAdapter);
        //setting up listener for the gridView
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(flipping == true){
                    return;
                }
                if(firstFlip){
                    card1 = cards.get(i);
                    card1.setTurned(true);
                    cardAdapter.notifyDataSetChanged();
                    firstFlip = false;
                }
                else if(firstFlip == false){
                    card2 = cards.get(i);
                    card2.setTurned(true);
                    cardAdapter.notifyDataSetChanged();

                    // if any of the cards has already been matched, then we don't proceed
                    if(card1.getCardMatched() == false && card2.getCardMatched() == false && card1 != card2){
                        firstFlip = true;
                        //if the cards' imageIDs' match and the cardIDs' differ, then proceed with adding points
                        if(card1.getImageID() == card2.getImageID() && card1.getCardID() != card2.getCardID()){
                            gameScore += 5;
                            matchCount++;
                            updateScore(gameScore);
                            //making sure the user doesn't abuse the system
                            cards.get(card1.getCardID()).setCardMatched(true);
                            cards.get(card2.getCardID()).setCardMatched(true);
                            if(matchCount == 8){
                                getUserName();
                            }
                        }
                        //if the imageIDs' differ, then take away a point
                        else if (card1.getImageID() != card2.getImageID()){
                            flipping = true;
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    card1.setTurned(false);
                                    card2.setTurned(false);
                                    cardAdapter.notifyDataSetChanged();
                                    flipping = false;
                                    gameScore--;
                                    updateScore(gameScore);
                                }
                            }, 1000);
                        }
                    }
                    //safety checks to make sure the user does not abuse the game mechanic
                    else if(card1.getCardID() == card2.getCardID() && card1.getCardMatched() == false){
                        card1.setTurned(false);
                        cardAdapter.notifyDataSetChanged();
                        card1 = null;
                        card2 = null;
                        firstFlip = true;
                        gameScore--;
                        updateScore(gameScore);
                    }
                    else if(card1.getCardID() == card2.getCardID() && card1.getCardMatched() == true){
                        card1 = null;
                        card2 = null;
                        firstFlip = true;
                    }
                    else if(card1.getCardMatched() == true && card2.getCardMatched() == false){
                        card2.setTurned(false);
                        cardAdapter.notifyDataSetChanged();
                        card1 = null;
                        card2 = null;
                        firstFlip = true;
                        gameScore--;
                        updateScore(gameScore);
                    }
                    else if(card1.getCardMatched() == false && card2.getCardMatched() == true){
                        card1.setTurned(false);
                        cardAdapter.notifyDataSetChanged();
                        card1 = null;
                        card2 = null;
                        firstFlip = true;
                        gameScore--;
                        updateScore(gameScore);
                    }
                    else{
                        card2.setTurned(false);
                        card1.setTurned(false);
                        cardAdapter.notifyDataSetChanged();
                        card1 = null;
                        card2 = null;
                        firstFlip = true;
                    }
                }
            }
        });
    }

    private void getUserName() {
        gridView.setVisibility(View.INVISIBLE);
        finalText.setVisibility(View.VISIBLE);
        confirmButton.setVisibility(View.VISIBLE);
        //highScore.setVisibility(View.VISIBLE);
        //editText.setVisibility(View.VISIBLE);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userName = editText.getText().toString();
                userScore = gameScore;
                finalText.setVisibility(View.INVISIBLE);
                editText.setVisibility(View.INVISIBLE);
                confirmButton.setVisibility(View.INVISIBLE);
                gridView.setVisibility(View.VISIBLE);
            }
        });
    }

    //fill the card array with Card class elements
    private void populateCardArray() {
        for (int i = 0; i < images.size(); i++){
            cards.add(new Card(images.get(i), i));
        }
    }

    //fill the image array with image id elements
    private void populateArrays() {
        for (int i = 0; i < 2; i++){
            images.add(R.drawable.card_cc);
            images.add(R.drawable.card_cloud);
            images.add(R.drawable.card_console);
            images.add(R.drawable.card_multiscreen);
            images.add(R.drawable.card_remote);
            images.add(R.drawable.card_tablet);
            images.add(R.drawable.card_tv);
            images.add(R.drawable.card_vr);
        }
    }

    //method used to update the score textView
    public void updateScore(int score){
        currentScore.setText("Current \nscore: " + score);
    }

}